<center>
<h1> Lip Reading using Deep Neural Networks </h1>
<h2> Bachelor thesis at FIT CTU </h2>
</center>

</br>
The goal of this project was to train a deep neural network capable of recognizing individual English words being articulated in front of a camera in browser, solely by using image frames (ignoring audio).

It is based on 'Lip Reading in the Wild' by Joon Son Chung and Andrew Zisserman
Using dataset by BBC-Oxford
http://www.robots.ox.ac.uk/~vgg/data/lip_reading/

Web application preview:
![web-app](http://janhorak.info/lip/img/web-demo.png)
